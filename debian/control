Source: nsdiff
Section: net
Priority: optional
Maintainer: Daniel Gröber <dxld@darkboxed.org>
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 13),
Standards-Version: 4.6.2
Homepage: https://dotat.at/prog/nsdiff/
Vcs-Git: https://salsa.debian.org/debian/nsdiff.git
Vcs-Browser: https://salsa.debian.org/debian/nsdiff

Package: nsdiff
Architecture: all
Depends:
 ${perl:Depends},
 bind9-utils,
 bind9-dnsutils,
 ${misc:Depends},
Description: generate nsupdate script from DNS zone differences
 The nsdiff program compares two versions of a DNS zone, and outputs the
 differences between them as a dynamic DNS script for use with the nsupdate
 program.
 .
 This provides an elegant way to merge hand written static zone files into
 otherwise dynamic zones.
 .
 nsdiff supports comparing on-disk zone-files and retreiving both sides
 via standard AXFR zone transfer, optionally autenticated with a TSIG
 key.
